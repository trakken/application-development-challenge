""" Base tests of this coding challenge. All tests should execute successfully! """
import os
import uuid
import json
import pytest

from datetime import date, datetime, timedelta

from src import main


@pytest.fixture
def tmp_dir():
    return f"./{uuid.uuid4()}"


@pytest.fixture
def years():
    return ["1992", "1993", "2000", "2005"]


@pytest.fixture
def setup(tmp_dir, years):
    os.mkdir(tmp_dir)
    years = "year\n" + "\n".join(years)
    with open(f"{tmp_dir}/years.csv", "w") as years_file:
        years_file.write(years)
    yield

    # tear down
    for root, dirs, files in os.walk(tmp_dir):
        for name in files:
            os.remove(os.path.join(root, name))

    tmp_dir = os.rmdir(tmp_dir)


@pytest.fixture
def run_main(tmp_dir, setup):
    main.main(tmp_dir + "/years.csv", tmp_dir)


def test_files_exists(tmp_dir, years, run_main):
    """
    Test if all files are created correctly
    """
    for year in years:
        assert os.path.exists(f"{tmp_dir}/{year}.json")


def test_file_format(tmp_dir, years, run_main):
    """
    Test if all files are formatted correctly
    """
    for year in years:
        data = json.load(open(tmp_dir + f"/{year}.json"))

        assert len(data) == 12

        for month, days in data.items():
            assert {"first", "last"} == days.keys()


def test_first_of_month(tmp_dir, years, run_main):
    """
    Test if the first of the month is correct in each file
    """
    for year in years:
        data = json.load(open(tmp_dir + f"/{year}.json"))

        for month in data:

            assert data[month]["first"] == date(int(year), int(month), 1).strftime(
                "%d-%m-%Y"
            )


def test_last_of_month(tmp_dir, years, run_main):
    """
    Test if the last of the month is correct in each file
    """
    for year in years:
        data = json.load(open(tmp_dir + f"/{year}.json"))

        for month in data:
            last = datetime.strptime(data[month]["last"], "%d-%m-%Y")
            first = last + timedelta(days=1)
            assert first.day == 1
